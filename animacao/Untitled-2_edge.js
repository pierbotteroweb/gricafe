/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='images/',
        aud='media/',
        vid='media/',
        js='js/',
        fonts = {
        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: 'contatogricafe',
                            type: 'image',
                            rect: ['0px', '0px', '450px', '250px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im+"contatogricafe.jpg",'0px','0px'],
                            transform: [[],[],[],['0.01','0.01']]
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: [undefined, undefined, '450px', '250px'],
                            overflow: 'hidden',
                            fill: ["rgba(104,18,43,1.00)"]
                        }
                    }
                },
                timeline: {
                    duration: 30000,
                    autoPlay: true,
                    data: [
                        [
                            "eid4",
                            "scaleY",
                            0,
                            5000,
                            "linear",
                            "${contatogricafe}",
                            '0.01',
                            '1'
                        ],
                        [
                            "eid6",
                            "scaleY",
                            30000,
                            0,
                            "linear",
                            "${contatogricafe}",
                            '1',
                            '1'
                        ],
                        [
                            "eid3",
                            "scaleX",
                            0,
                            5000,
                            "linear",
                            "${contatogricafe}",
                            '0.01',
                            '1'
                        ],
                        [
                            "eid5",
                            "scaleX",
                            30000,
                            0,
                            "linear",
                            "${contatogricafe}",
                            '1',
                            '1'
                        ]
                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

    if (!window.edge_authoring_mode) AdobeEdge.getComposition(compId).load("Untitled-2_edgeActions.js");
})("EDGE-1054966601");
