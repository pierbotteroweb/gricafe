$(document).ready(function(){
  $("#minilogo").fadeOut();
  $("#ulCardapios").fadeOut();
  $(window).scroll(function() {
    if ($(document).scrollTop() > 50) {
      $("#minilogo").fadeIn(1000);
      $("#gricafe").fadeOut(1000);
    } else {
      $("#minilogo").fadeOut(1000);
      $("#gricafe").fadeIn(1000);
    };
    if ($(document).scrollTop() > 800) {
      $("#ulCardapios").fadeIn(1000);
    } else {
      $("#ulCardapios").fadeOut(1000);
      $("#navcardapios ul li").removeClass("underline");
    };
  });
	var eventos = $("#apresent").offset().top;
	var home = $("body").offset().top;
	var navcardapios = $("#navcardapios").offset().top;
	var contato = $("#contato").offset().top;
	
	// When #scroll is clicked
	$("#linkEventos").click(function(){
		// Scroll down to the needed position 
		$("html, body").animate({scrollTop:eventos -50}, "slow");
		// Stop the link from acting like a normal anchor link
		return false;
	});

	$("#linkHome").click(function(){
		$("html, body").animate({scrollTop:home}, "slow");
		return false;
	});

	$("#linkCardapios").click(function(){
		$("html, body").animate({scrollTop:navcardapios -50}, "slow");
		return false;
	});

	$("#linkContato").click(function(){
		$("html, body").animate({scrollTop:contato -50}, "slow");
		return false;
	});
	$("#linkExtras").click(function(){
      $("#gricafe").fadeOut(1000);
	});
	$("#carPierMenu li:first").addClass("atual");
	$("#carPierMenu li").click(function(){
        $("#carPierMenu li").removeClass("atual");
        $(this).addClass("atual");
    });
	$("#navcardapios ul li").click(function(){
        $("#navcardapios ul li").removeClass("underline");
        $(this).addClass("underline");
    });

});
