/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='images/',
        aud='media/',
        vid='media/',
        js='js/',
        fonts = {
        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: 'contatogricafe',
                            type: 'image',
                            rect: ['201px', '112px', '451px', '250px', 'auto', 'auto'],
                            opacity: '0',
                            fill: ["rgba(0,0,0,0)",im+"contatogricafe.jpg",'0px','0px']
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: [undefined, undefined, '450px', '250px'],
                            overflow: 'hidden',
                            fill: ["rgba(104,18,43,1.00)"]
                        }
                    }
                },
                timeline: {
                    duration: 2000,
                    autoPlay: true,
                    data: [
                        [
                            "eid19",
                            "opacity",
                            0,
                            2000,
                            "linear",
                            "${contatogricafe}",
                            '0',
                            '1'
                        ],
                        [
                            "eid17",
                            "width",
                            0,
                            2000,
                            "linear",
                            "${contatogricafe}",
                            '47px',
                            '451px'
                        ],
                        [
                            "eid16",
                            "left",
                            0,
                            2000,
                            "linear",
                            "${contatogricafe}",
                            '201px',
                            '1px'
                        ],
                        [
                            "eid18",
                            "top",
                            0,
                            2000,
                            "linear",
                            "${contatogricafe}",
                            '112px',
                            '-1px'
                        ],
                        [
                            "eid15",
                            "height",
                            0,
                            2000,
                            "linear",
                            "${contatogricafe}",
                            '26px',
                            '250px'
                        ],
                        [
                            "eid2",
                            "scaleY",
                            0,
                            0,
                            "linear",
                            "${contatogricafe}",
                            '1',
                            '1'
                        ],
                        [
                            "eid1",
                            "scaleX",
                            0,
                            0,
                            "linear",
                            "${contatogricafe}",
                            '1',
                            '1'
                        ]
                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

    if (!window.edge_authoring_mode) AdobeEdge.getComposition(compId).load("contatogricafe1_edgeActions.js");
})("EDGE-621418326");
