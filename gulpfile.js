var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var sass = require('gulp-sass');
var concat = require('gulp-concat');

gulp.task('sass',function(){
	return gulp.src('sass/*.scss')
	.pipe(sass())
	.pipe(gulp.dest('css'))
	.pipe(browserSync.stream());
});

gulp.task('browser',function(){
	browserSync.init({
		server: "./"
	});
	gulp.watch('./**.*')
});

gulp.task('montagem',function(){
	function montaPagina(modulo,final){
		return gulp.src(['src/modules/_header.html','src/modules/'+modulo,'src/modules/_footer.html'])
		.pipe(concat(final))
		.pipe(gulp.dest(''))
	}

	montaPagina('_teste.html','teste.html');
});

gulp.task('default',['montagem','browser','sass'],function(){
	gulp.watch("*.html").on('change',browserSync.reload);
	gulp.watch("sass/*.scss",['sass']);
	gulp.watch('src/modules/*.html',['montagem'])
});

